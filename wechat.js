const appium = require('selenium-appium');
const wd = require('selenium-webdriver');
const { By2, driver, WebDriver2 } = appium;
const { Builder, until } = wd;

const capabilities = {
    browserName: '',
    platformName: 'windows',
    deviceName: 'WindowsPC',
    app: 'C://Program Files (x86)//Tencent//WeChat//WeChat.exe'
}

const url = 'http://localhost:4723/wd/hub'

async function send(msg) {
    const webdriver = await new Builder()
        .usingServer(url)
        .withCapabilities(capabilities)
        .build();
    // const element = await webdriver.wait(until.elementLocated(By2.nativeName('Search')));
    // console.log(element)
    // await element.click();
    // element.sendKeys('txjw ');
    // const txjwElement = await webdriver.wait(until.elementLocated(By2.nativeName('<em>天下军武</em>')));
    // txjwElement.click();
    // const moreElement = await webdriver.wait(until.elementLocated(By2.nativeName('More')));
    // moreElement.click();
    // const officialElement = await webdriver.wait(until.elementLocated(By2.nativeName('Enter Official Account')));
    // officialElement.click();


    const element = await webdriver.wait(until.elementLocated(By2.nativeName('Enter')));
    console.log(element)
    await element.click();
    await element.sendKeys(msg);
    const sendElement = await webdriver.wait(until.elementLocated(By2.nativeName('Send (S)')));
    // element.sendKeys('侯印不闻封李广@北');
    sendElement.click();
}

module.exports = { send };

