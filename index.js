const notifier = require('node-notifier');
const path = require('path');
const puppeteer = require('puppeteer');
var fs = require('fs');
const wechat = require('./wechat');

(async () => {
    try {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        page.setDefaultTimeout(60000)
        await page.goto('http://xiage.yy.com/forum-219-1.html', { waitUntil: 'networkidle2' });
        const content = await page.content();
        const href = content.match(/<a href="(th.*)" onclick.*>.*腿.*<\/a>/);
        if (href && href[1]) {
            await page.goto(`http://xiage.yy.com/${href[1]}`);
            const element = await page.$('.t_f');
            const text = await page.evaluate(element => element.textContent, element);
            const msg = text.trim().split('@')[0] + '@北';
            console.log(msg)
            if (msg) {
                wechat.send(msg);
                sendNotification(msg)
                fs.writeFile('C:\\Users\\yli\\Desktop\\今日蚊子腿.txt', msg, function (err) {
                    if (err) throw err;
                });
            } else {
                sendNotification('未发现蚊子腿！！！！！');
            }
        } else {
            sendNotification('未发现蚊子腿！！！！！');
        }

        // console.log(href)
        await browser.close();
    } catch(e) {
        console.log(e)
        sendNotification('获取失败！！！！！！！！' + e.message);
    }

})();

function sendNotification(msg) {
    notifier.notify(
        {
            title: '今日蚊子腿',
            message: msg,
            icon: path.join(__dirname, 'wot.png'), // Absolute path (doesn't work on balloons)
            sound: true, // Only Notification Center or Windows Toasters
            wait: true // Wait with callback, until user action is taken against notification, does not apply to Windows Toasters as they always wait or notify-send as it does not support the wait option
        },
        function (err, response) {
            // Response is response from notification
        }
    );
}

